import { Component, OnInit } from '@angular/core';
import { Cliente } from './Cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private cliente: Cliente = new Cliente()
  private titulo:string = 'Crear cliente'
  private errores: string[]

  constructor(private _clientService: ClienteService, 
              private _router: Router,
              private _activatedRouter: ActivatedRoute) { }

  ngOnInit() {
    this.cargarCliente()
  }

  cargarCliente():void{
    this._activatedRouter.params.subscribe(params => {
      let id = params['id']
      if (id){
        this._clientService.getCliente(id).subscribe((cliente) => this.cliente = cliente)
      }
    })

  }

  create():void{
  this._clientService.create(this.cliente).subscribe(
    json => {
      this._router.navigate(['/clientes'])
      swal.fire('Nuevo cliente', `Cliente ${json.cliente.nombre} creado con éxito!`, 'success')
    },
    err => {
      this.errores = err.error.errors as string[]
    }
  );
  }

  update():void{
    this._clientService.update(this.cliente)
    .subscribe( cliente => {
      this._router.navigate(['/clientes'])
      swal.fire('Cliente Actualizado', `Cliente ${cliente.nombre} actualizado con ExtensionScriptApis`, 'success')
    },
    err => {
      this.errores = err.error.errors as string[]
    } )
  }

}
