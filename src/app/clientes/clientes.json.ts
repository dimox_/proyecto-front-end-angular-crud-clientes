import { Cliente } from './Cliente'

export const CLIENTES: Cliente[] = [

    { id:1, nombre:'Jose', apellido:'Manzan', createAt:'2019-01-01', email:'jmannf@gmail.com' },
    { id:2, nombre:'Ana', apellido:'Lopez', createAt:'2019-01-01', email:'pepo@gmail.com'},
    { id:3, nombre:'Pepa', apellido:'Miranda', createAt:'2019-01-01', email:'rodr@gmail.com'},
    { id:4, nombre:'Antonio', apellido:'Lucero', createAt:'2019-01-01', email:'reps@gmail.com'}

];