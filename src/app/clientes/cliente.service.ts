import { Injectable } from '@angular/core';
import { CLIENTES } from './clientes.json';
import { Cliente } from './Cliente';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ClientesComponent } from './clientes.component.js';
import { formatDate, DatePipe } from '@angular/common';
import localeES from '@angular/common/locales/es';

@Injectable()
export class ClienteService {

  private urlEndPoint:string = 'http://localhost:8080/api/clientes'
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'})

  constructor( private _http: HttpClient,
              private _router:Router ) { }

  getClientes(): Observable<Cliente[]> {
   // return of(CLIENTES);
    return this._http.get(this.urlEndPoint).pipe(
      map(response => {
        let clientes = response as Cliente[];
        return clientes.map(cliente => {
          cliente.nombre = cliente.nombre.toUpperCase();
         
          let datePipe = new DatePipe('es')
          // cliente.createAt = datePipe.transform(cliente.createAt, 'EEEE dd, MMMM yyyy')  // formatDate( cliente.createAt, 'dd-MM-yyyy', 'en-US')
          return cliente;
        })
      })
    );
  }

  create(cliente:Cliente): Observable<any>{
    return this._http.post<Cliente>(this.urlEndPoint, cliente, {headers: this.httpHeaders})
    .pipe(catchError(e => {
      if (e.status == 400){
        return throwError(e);
      }
      console.log(e.error.mensaje);
      Swal.fire(e.error.mensaje, e.error.error, 'error');
      return throwError(e);
    }))
  }

  getCliente(id:number): Observable<Cliente>{
    return this._http.get<Cliente>(`${this.urlEndPoint}/${id}`)
    .pipe(catchError(e => {
      this._router.navigate(['/clientes']);
      console.log(e.error.mensaje);
      Swal.fire('Error al editar', e.error.mensaje, 'error');
      return throwError(e);
    }))
  }

  update(cliente:Cliente): Observable<Cliente>{
    return this._http.put<Cliente>(`${this.urlEndPoint}/${cliente.id}`, cliente, {headers: this.httpHeaders})
    .pipe(catchError(e => {
      if (e.status == 400){
        return throwError(e);
      }
      console.log(e.error.mensaje);
      Swal.fire(e.error.mensaje, e.error.error, 'error');
      return throwError(e);
    }))
                            
  }

  delete(id: number): Observable<Cliente>{
   return this._http.delete<Cliente>(`${this.urlEndPoint}/${id}`, {headers: this.httpHeaders})
   .pipe(catchError(e => {
    console.log(e.error.mensaje);
    Swal.fire(e.error.mensaje, e.error.error, 'error');
    return throwError(e);
  }))
  }

}
